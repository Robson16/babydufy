<?php

/**
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

/**
 * Enqueue styles and scripts for the theme
 */
function babydufy_scripts()
{
	// CSS
	wp_enqueue_style('babydufy-webfonts', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('babydufy-shared-styles', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('babydufy-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/frontend-styles.css', array(), wp_get_theme()->get('Version'));

	// Js
	wp_enqueue_script('comment-reply');
	wp_enqueue_script('babydufy-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend-bundled.js', array(), wp_get_theme()->get('Version'), true);
}
add_action('wp_enqueue_scripts', 'babydufy_scripts');

/**
 * Gutenberg scripts
 * @see https://www.billerickson.net/block-styles-in-gutenberg/
 */
function babydufy_gutenberg_scripts()
{
	// Web Fonts
	wp_enqueue_style('babydufy-webfonts', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get('Version'));
}
add_action('enqueue_block_editor_assets', 'babydufy_gutenberg_scripts');

/**
 * Set theme defaults and register support for various WordPress features.
 */
function babydufy_setup()
{
	// Enabling translation support
	$textdomain = 'babydufy';
	load_theme_textdomain($textdomain, get_stylesheet_directory() . '/languages/');
	load_theme_textdomain($textdomain, get_template_directory() . '/languages/');

	// Customizable logo
	add_theme_support('custom-logo', array(
		'height'      => 62,
		'width'       => 126,
		'flex-height' => false,
		'flex-width'  => false,
		'header-text' => array('site-title', 'site-description'),
	));


	// Custom Header
	add_theme_support('custom-header', array(
		'default-image'      => get_template_directory_uri() . '/assets/images/default-header.jpg',
		'default-text-color' => '4581c1',
		'width'              => 1920,
		'height'             => 460,
		'flex-width'         => true,
		'flex-height'        => true,
	));

	// Custom background
	add_theme_support('custom-background', array(
		'default-color'          => '4581c1',
		'default-image'          => get_template_directory_uri() . '/assets/images/default-background.jpg',
		'default-repeat'         => 'no-repeat',
		'default-position-x'     => 'center',
		'default-position-y'     => 'center',
		'default-size'           => 'cover',
		'default-attachment'     => 'scroll',
		'wp-head-callback'       => '_custom_background_cb',
		'admin-head-callback'    => '',
		'admin-preview-callback' => ''
	));

	// Menu registration
	register_nav_menus(array(
		'main_menu' => __('Main Menu', 'babydufy'),
		'footer_menu' => __('Footer Menu', 'babydufy'),
	));

	// Load custom styles in the editor.
	add_theme_support('editor-styles');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/shared/shared-styles.css');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/admin/editor-styles.css');

	// Let WordPress manage the document title.
	add_theme_support('title-tag');

	// Enable support for featured image on posts and pages.
	add_theme_support('post-thumbnails');

	// Enable support for embedded media for full weight
	add_theme_support('responsive-embeds');

	// Enables wide and full dimensions
	add_theme_support('align-wide');

	// Standard style for each block.
	add_theme_support('wp-block-styles');

	// Creates the specific color palette
	add_theme_support('editor-color-palette', array(
		array(
			'name'  => __('White', 'babydufy'),
			'slug'  => 'white',
			'color' => '#ffffff',
		),
		array(
			'name'  => __('Baby Blue Eyes', 'babydufy'),
			'slug'  => 'baby-blue-eyes',
			'color' => '#a5d4ff',
		),
		array(
			'name'  => __('Steel Blue', 'babydufy'),
			'slug'  => 'steel-blue',
			'color' => '#4581c1',
		),
		array(
			'name'  => __('Permanent Geranium Lake', 'babydufy'),
			'slug'  => 'permanent-geranium-lake',
			'color' => '#e2282a',
		),
		array(
			'name'  => __('Dim Gray', 'babydufy'),
			'slug'  => 'dim-gray',
			'color' => '#6e6e6e',
		),
		array(
			'name'  => __('Black', 'babydufy'),
			'slug'  => 'black',
			'color' => '#000000',
		),
	));

	// Custom font sizes.
	add_theme_support('editor-font-sizes', array(
		array(
			'name' => __('Small', 'babydufy'),
			'size' => 16,
			'slug' => 'small',
		),
		array(
			'name' => __('Normal', 'babydufy'),
			'size' => 18,
			'slug' => 'normal',
		),
		array(
			'name' => __('Medium', 'babydufy'),
			'size' => 24,
			'slug' => 'medium',
		),
		array(
			'name' => __('Big', 'babydufy'),
			'size' => 40,
			'slug' => 'big',
		),
		array(
			'name' => __('Huge', 'babydufy'),
			'size' => 50,
			'slug' => 'huge',
		),
	));

	/**
	 * Custom blocks styles.
	 *
	 * @see https://wpblockz.com/tutorial/register-block-styles-in-wordpress/
	 * @link https://developer.wordpress.org/block-editor/reference-guides/filters/block-filters/
	 */
	register_block_style('core/gallery', array(
		'name' => 'rounded-border',
		'label' => __('Rounded Border', 'babydufy'),
	));
}
add_action('after_setup_theme', 'babydufy_setup');

/**
 * Remove website field from comment form
 *
 */
function babydufy_website_remove($fields)
{
	if (isset($fields['url']))
		unset($fields['url']);
	return $fields;
}
add_filter('comment_form_default_fields', 'babydufy_website_remove');

/**
 *  TGM Plugin
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';

/**
 *  Kirki Framework Config
 */
require_once get_template_directory() . '/includes/kirki/kirki-config.php';

/**
 *  WooCommerce Functions
 */
if (class_exists('WooCommerce')) {
	require_once get_template_directory() . '/includes/woocommerce-functions.php';
}

/**
 *  Kadence Blocks
 */
if (class_exists('Kadence_Blocks_Frontend')) {
	require_once get_template_directory() . '/includes/kadence-blocks-functions.php';
}
