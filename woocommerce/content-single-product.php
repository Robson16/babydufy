<?php

/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action('woocommerce_before_single_product');

if (post_password_required()) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class('', $product); ?>>

	<?php
	/**
	 * Hook: woocommerce_before_single_product_summary.
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 * @hooked woocommerce_show_product_images - 20
	 */
	do_action('woocommerce_before_single_product_summary');
	?>

	<div class="summary entry-summary">
		<?php
		/**
		 * Hook: woocommerce_single_product_summary.
		 *
		 * @hooked woocommerce_template_single_title - 5
		 * @hooked woocommerce_template_single_rating - 10
		 * @hooked woocommerce_template_single_price - 10
		 * @hooked woocommerce_template_single_excerpt - 20
		 * @hooked woocommerce_template_single_add_to_cart - 30
		 * @hooked woocommerce_template_single_meta - 40
		 * @hooked woocommerce_template_single_sharing - 50
		 * @hooked WC_Structured_Data::generate_product_data() - 60
		 */
		do_action('woocommerce_single_product_summary');
		?>

		<?php
		$partner_store_link = array(
			'americanas' => rwmb_get_value('babydufy_product_partner_store_americanas'),
			'magazineluiza' => rwmb_get_value('babydufy_product_partner_store_magazineluiza'),
			'mercadolivre' => rwmb_get_value('babydufy_product_partner_store_mercadolivre'),
			'shopee' => rwmb_get_value('babydufy_product_partner_store_shopee'),
			'submarino' => rwmb_get_value('babydufy_product_partner_store_submarino'),
		);

		if (
			$partner_store_link['americanas'] ||
			$partner_store_link['magazineluiza'] ||
			$partner_store_link['mercadolivre'] ||
			$partner_store_link['shopee'] ||
			$partner_store_link['submarino']
		) :
		?>
			<div class="partner-stores">
				<h2><?php _e('Shop at our partner stores', 'babydufy') ?></h2>

				<div class="partner-stores-logos">
					<?php if ($partner_store_link['americanas']) : ?>
						<a href="<?php echo $partner_store_link['americanas']; ?>" target="_blank" rel="noopener">
							<img src="<?php echo get_template_directory_uri() . '/assets/images/americanas-logo.png'; ?>" alt="Americanas" title="Americanas" />
						</a>
					<?php endif; ?>

					<?php if ($partner_store_link['magazineluiza']) : ?>
						<a href="<?php echo $partner_store_link['magazineluiza']; ?>" target="_blank" rel="noopener">
							<img src="<?php echo get_template_directory_uri() . '/assets/images/magalu-logo.png'; ?>" alt="Magazine Luiza" title="Magazine Luiza" />
						</a>
					<?php endif; ?>

					<?php if ($partner_store_link['mercadolivre']) : ?>
						<a href="<?php echo $partner_store_link['mercadolivre']; ?>" target="_blank" rel="noopener">
							<img src="<?php echo get_template_directory_uri() . '/assets/images/mercadolivre-logo.png'; ?>" alt="Mercado Livre" title="Mercado Livre" />
						</a>
					<?php endif; ?>

					<?php if ($partner_store_link['shopee']) : ?>
						<a href="<?php echo $partner_store_link['shopee']; ?>" target="_blank" rel="noopener">
							<img src="<?php echo get_template_directory_uri() . '/assets/images/shopee-logo.png'; ?>" alt="Shopee" title="Shopee" />
						</a>
					<?php endif; ?>

					<?php if ($partner_store_link['submarino']) : ?>
						<a href="<?php echo $partner_store_link['submarino']; ?>" target="_blank" rel="noopener">
							<img src="<?php echo get_template_directory_uri() . '/assets/images/submarino-logo.png'; ?>" alt="Submarino" title="Submarino" />
						</a>
					<?php endif; ?>
				</div>
				<!-- /.partner-stores-logos -->
			</div>
			<!-- /.partner-stores -->
		<?php endif; ?>

	</div>

	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action('woocommerce_after_single_product_summary');
	?>
</div>

<?php do_action('woocommerce_after_single_product'); ?>
