<?php

/**
 * The template for displaying the footer
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

?>

<footer class="footer">

	<div class="container">
		<?php
		wp_nav_menu(array(
			'theme_location' => 'footer_menu',
			'depth' => 1,
			'container_class' => 'footer-menu-wrap',
			'menu_class' => 'footer-menu',
		));
		?>
	</div>
	<!-- /.container -->

	<div class="footer-main container">
		<div>
			<?php
			if (get_theme_mod("babydufy_setting_footer_logo")) {
				echo wp_get_attachment_image(get_theme_mod("babydufy_setting_footer_logo"));
			} else {
				echo '<h1 style="margin: 0;">' . get_bloginfo('title') . '</h1>';
			}
			?>
		</div>

		<div class="footer-contacts">
			<?php if (get_theme_mod("babydufy_setting_phone")) : ?>
				<div>
					<img src="<?php echo get_template_directory_uri() . '/assets/images/icon-phone-white.png'; ?>" alt="Icon">
					<a href="<?php echo "tel:+55" . preg_replace('/\D+/', '', get_theme_mod("babydufy_setting_phone")); ?>">
						<?php echo get_theme_mod("babydufy_setting_phone"); ?>
					</a>
				</div>
			<?php endif; ?>

			<?php if (get_theme_mod("babydufy_setting_email")) : ?>
				<div>
					<img src="<?php echo get_template_directory_uri() . '/assets/images/icon-mail-white.png'; ?>" alt="Icon">
					<a href="<?php echo "mailto:" . get_theme_mod("babydufy_setting_email"); ?>">
						<?php echo get_theme_mod("babydufy_setting_email"); ?>
					</a>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<!-- /.container-fluid -->

	<div class="footer-copyright">
		<div class="container">
			<span>&copy;&nbsp;<?php echo bloginfo('title'); ?>&nbsp;<?php echo wp_date('Y') . '.'; ?>&nbsp;<?php _e('All rights reserved.', 'babydufy') ?></span>
			<span>
				<?php _e('Developed by', 'babydufy') ?>
				&nbsp;
				<a href="https://agenciabeb.com.br/" target="_blank" rel="noopener">
					<img src="<?php echo get_template_directory_uri() . '/assets/images/agenciabeb.png'; ?>" alt="Agência B&B">
				</a>
			</span>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.footer-copyright -->

</footer>

<?php
if (class_exists('WooCommerce')) {
	get_template_part('partials/modal/modal', 'product-preview');
}
?>

<?php wp_footer(); ?>

</body>

</html>
