<?php

/**
 * Custom WordPress Rest API EndPoint to return a
 * WooCommerce product preview data for a given ID
 *
 * Ex:
 * <your-domain>/wp-json/babydufy/v1/product/<product-id>
 *
 * @see https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-custom-endpoints/
 *
 * @param int id
 * @return object
 *
 */

add_action('rest_api_init', function () {
	register_rest_route('babydufy/v1', '/product/(?P<id>\d+)', array(
		'methods' => 'GET',
		'callback' => 'babydufy_wc_product_preview_data',
		'permission_callback' => function () {
			return true;
		},
		'args' => array(
			'id' => array(
				'required'          => true,
				'validate_callback' => function ($param, $request, $key) {
					return is_numeric($param);
				}
			),
		),
	));
});

/**
 * Get the WooCommerce product data for the given product ID
 */
function babydufy_wc_product_preview_data(WP_REST_Request $request)
{
	$productId = $request['id'];

	$product = wc_get_product($productId);

	if (empty($product)) {
		return new WP_Error('product_notfound', __('Product not found.', 'babydufy'), array('status' => 404));
	}

	$partner_stores = array();

	if (rwmb_get_value('babydufy_product_partner_store_americanas', array(), $productId)) {
		array_push($partner_stores, array(
			'name' => 'Americanas',
			'logo' => get_template_directory_uri() . '/assets/images/americanas-logo.png',
			'link' => rwmb_get_value('babydufy_product_partner_store_americanas', array(), $productId),
		));
	}

	if (rwmb_get_value('babydufy_product_partner_store_magazineluiza', array(), $productId)) {
		array_push($partner_stores, array(
			'name' => 'Magazine Luiza',
			'logo' => get_template_directory_uri() . '/assets/images/magalu-logo.png',
			'link' => rwmb_get_value('babydufy_product_partner_store_magazineluiza', array(), $productId),
		));
	}

	if (rwmb_get_value('babydufy_product_partner_store_mercadolivre', array(), $productId)) {
		array_push($partner_stores, array(
			'name' => 'Mercado Livre',
			'logo' => get_template_directory_uri() . '/assets/images/mercadolivre-logo.png',
			'link' =>	rwmb_get_value('babydufy_product_partner_store_mercadolivre', array(), $productId),
		));
	}

	if (rwmb_get_value('babydufy_product_partner_store_shopee', array(), $productId)) {
		array_push($partner_stores, array(
			'name' => 'Shopee',
			'logo' => get_template_directory_uri() . '/assets/images/shopee-logo.png',
			'link' =>	rwmb_get_value('babydufy_product_partner_store_shopee', array(), $productId),
		));
	}

	if (rwmb_get_value('babydufy_product_partner_store_submarino', array(), $productId)) {
		array_push($partner_stores, array(
			'name' => 'Submarino',
			'logo' => get_template_directory_uri() . '/assets/images/submarino-logo.png',
			'link' =>	rwmb_get_value('babydufy_product_partner_store_submarino', array(), $productId),
		));
	}

	$data['status'] = 200;
	$data['message'] = __('Product found.', 'babydufy');
	$data['data'] = array(
		'id'				=> $productId,
		'image_url'			=> wp_get_attachment_image_src($product->get_image_id(), 'medium')[0],
		'type' 				=> $product->get_type(),
		'name' 				=> $product->get_name(),
		'slug' 				=> $product->get_slug(),
		'short_description'	=> $product->get_short_description(),
		'sku'				=> $product->get_sku(),
		'add_to_cart_url' 	=> ($product->is_type('external')) ? $product->add_to_cart_url() : get_permalink($productId),
		'add_to_cart_text' 	=> $product->add_to_cart_text(),
		'partner_stores' 	=> $partner_stores,
	);

	$response = new WP_REST_Response($data);
	$response->set_status(200);

	return $response;
}
