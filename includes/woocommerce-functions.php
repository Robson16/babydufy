<?php

/**
 *
 * Functions to enhance and add features to the Woocommerce
 *
 */

/**
 * Enqueue styles and scripts for the theme
 */
add_action('wp_enqueue_scripts', 'babydufy_woocommerce_scripts');
function babydufy_woocommerce_scripts()
{
	// CSS
	wp_enqueue_style('babydufy-woocommerce-shared-styles', get_template_directory_uri() . '/assets/css/shared/woocommerce-shared-styles.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('babydufy-woocommerce-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/woocommerce-frontend-styles.css', array(), wp_get_theme()->get('Version'));
}

/**
 * Register Woocommerce defaults and support for various WordPress features.
 */
add_action('after_setup_theme', 'babydufy_woocommerce_setup');
function babydufy_woocommerce_setup()
{
	// Enable basic support for WooCommerce
	add_theme_support('woocommerce');

	// Enable product gallery slider
	add_theme_support('wc-product-gallery-slider');

	// Load custom styles in the editor.
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/shared/woocommerce-shared-styles.css');
}

/**
 * Remove the default WooCommerce external Buy Product button on individual Product page.
 */
remove_action('woocommerce_external_add_to_cart', 'woocommerce_external_add_to_cart', 30);

/**
 * This will take care of the Buy Product button below the external product on the Shop page.
 */
add_filter('woocommerce_loop_add_to_cart_link', 'babydufy_external_add_product_link', 10, 2);
function babydufy_external_add_product_link($link)
{
	global $product;

	if ($product->is_type('external')) {

		$link = sprintf(
			'<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s" target="_blank">%s</a>',
			esc_url($product->add_to_cart_url()),
			esc_attr(isset($quantity) ? $quantity : 1),
			esc_attr($product->id),
			esc_attr($product->get_sku()),
			esc_attr(isset($class) ? $class : 'button product_type_external'),
			esc_html($product->add_to_cart_text())
		);
	}

	return $link;
}

/**
 * Add the open in a new browser tab WooCommerce external Buy Product button.
 */
add_action('woocommerce_external_add_to_cart', 'babydufy_external_add_to_cart', 30);
function babydufy_external_add_to_cart()
{
	global $product;

	if (!$product->add_to_cart_url()) {
		return;
	}

	$product_url = $product->add_to_cart_url();
	$button_text = $product->single_add_to_cart_text();

	/**
	 * The code below outputs the edited button with target="_blank" added to the html markup.
	 */
	do_action('woocommerce_before_add_to_cart_button'); ?>

	<p class="cart">
		<a href="<?php echo esc_url($product_url); ?>" rel="nofollow" class="single_add_to_cart_button button alt" target="_blank">
			<?php echo esc_html($button_text); ?>
		</a>
	</p>

<?php do_action('woocommerce_after_add_to_cart_button');
}

/**
 * Product open link loop customized
 */
function babydufy_template_loop_product_link_open()
{
	global $product;

	$link = apply_filters('woocommerce_loop_product_link', get_the_permalink(), $product);

	$classes = "woocommerce-LoopProduct-link woocommerce-loop-product__link";

	if (get_post_meta($product->id, 'babydufy_product_open_preview', true)) {
		$classes .= " open-preview";
	}

	echo sprintf(
		'<a href="%s" class="%s" data-product_id="%s">',
		$link,
		$classes,
		esc_attr($product->id),
	);
};

/**
 * Custom Meta-boxes
 */
require_once get_template_directory() . '/includes/metaboxes/wc-products-metabox.php';

/**
 * Custom Shortcode
 */
require_once get_template_directory() . '/includes/shortcode/wc-recent-products.php';

/**
 * Custom API EndPoint
 */
require_once get_template_directory() . '/includes/api/wc-product-preview-data.php';
