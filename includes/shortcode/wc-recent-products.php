<?php

/**
 * WooCommerce recent products list
 */
add_shortcode('babydufy_wc_recent_products', 'babydufy_wc_recent_products_shortcode');
function babydufy_wc_recent_products_shortcode($atts)
{
	// Attributes
	$atts = shortcode_atts(
		array(
			'number_of_items'   => '6',
			'number_of_columns'	=> '3',
			'orderby'           => 'date',
			'order'             => 'ASC',
		),
		$atts
	);

	// Custom query
	$products = new WP_Query(array(
		'post_type'         => 'product',
		'lang'              => substr(get_language_attributes(), 6, 2),
		'post_status'       => 'publish',
		'orderby'           => $atts['orderby'],
		'order'             => $atts['order'],
		'posts_per_page'    => $atts['number_of_items'],
	));

	// Creating the markup
	$product_html = "<div class='woocommerce recent-products'>";
	$product_html .= "<ul class='products columns-" . $atts['number_of_columns'] . "' >";

	$loopCount = 1;
	while ($products->have_posts()) {
		$products->the_post();

		$isLastOfTheColumn = ($loopCount % $atts['number_of_columns'] === 0) ? 'last' : '';

		$linkClasses = "woocommerce-loop-product__link";

		if (get_post_meta(get_the_ID(), 'babydufy_product_open_preview', true)) {
			$linkClasses .= " open-preview";
		}

		$product_html .= "<li class='product " . $isLastOfTheColumn . "'>";
		$product_html .= "<a class='" . $linkClasses . "' href='" . get_permalink() . "' data-product_id='" .  get_the_ID() . "'>";
		$product_html .= get_the_post_thumbnail($products->the_ID, 'medium', array('alt' => get_the_title()));
		$product_html .= "<h2 class='woocommerce-loop-product__title'>" . get_the_title() . "</h2>";
		$product_html .= "</a>";
		$product_html .= "</li>";

		$loopCount++;
	}

	$product_html .= "</div>";
	$product_html .= "</ul>";

	// Reset the query postdata
	wp_reset_postdata();

	return $product_html;
}
