<?php

new \Kirki\Section(
	'babydufy_section_footer_logo',
	array(
		'title'       => esc_html__('Footer Logo', 'babydufy'),
		'description' => esc_html__('Logo image to be used on the footer of the site', 'babydufy'),
		'priority'    => 160,
	)
);

new \Kirki\Field\Image(
	array(
		'settings' => 'babydufy_setting_footer_logo',
		'label'    => esc_html__('Logo Image', 'babydufy'),
		'section'  => 'babydufy_section_footer_logo',
		'default'  => '',
		'choices'  => array(
			'save_as' => 'id',
		),
	)
);
