<?php

new \Kirki\Section(
	'babydufy_section_contacts',
	array(
		'title'       => esc_html__('Contacts', 'babydufy'),
		'description' => esc_html__('Contact informations.', 'babydufy'),
		'priority'    => 160,
	)
);

new \Kirki\Field\Text(
	array(
		'settings' => 'babydufy_setting_phone',
		'label'    => esc_html__('Phone', 'babydufy'),
		'section'  => 'babydufy_section_contacts',
		'default'  => '',
		'priority' => 10,
	)
);

new \Kirki\Field\Text(
	array(
		'settings' => 'babydufy_setting_email',
		'label'    => esc_html__('E-mail', 'babydufy'),
		'section'  => 'babydufy_section_contacts',
		'default'  => '',
		'priority' => 10,
	)
);
