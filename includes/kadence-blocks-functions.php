<?php

/**
 * Kadence Blocks Plugins config functions
 *
 * @link https://wordpress.org/plugins/kadence-blocks/
 */


/**
 * Register a custom font to can be used for the plugin
 *
 * @see https://www.kadencewp.com/kadence-blocks/documentation/advanced/adding-a-custom-font-to-kadence-blocks/
 */
add_filter('kadence_blocks_add_custom_fonts', 'babydufy_kadence_blocks_custom_fonts');
function babydufy_kadence_blocks_custom_fonts($system_fonts)
{
	$system_fonts['Futura Std'] = array(
		'fallback' => 'Verdana, Arial, sans-serif',
		'weights' => array(
			'400',
			'600',
			'900'
		),
	);

	$system_fonts['Fredoka One'] = array(
		'fallback' => 'Verdana, Arial, sans-serif',
		'weights' => array(
			'400'
		),
	);

	return $system_fonts;
}
