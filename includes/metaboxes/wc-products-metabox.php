<?php

/**
 * WooCommerce Products Custom Metabox Settings
 *
 * @see https://metabox.io/
 * @link https://metabox.io/online-generator/
 */

add_filter('rwmb_meta_boxes', 'babydufy_wc_product_register_meta_boxes');
function babydufy_wc_product_register_meta_boxes($meta_boxes)
{
	$prefix = 'babydufy_product_';

	$meta_boxes[] = [
		'title'      => esc_html__('Extra Fields', 'babydufy'),
		'id'         => 'babydufy-product-extra-fields',
		'post_types' => ['product'],
		'context'    => 'normal',
		'autosave'   => true,
		'fields'     => [
			[
				'type' => 'checkbox',
				'name' => esc_html__('Open Preview', 'babydufy'),
				'id'   => $prefix . 'open_preview',
				'desc' => esc_html__('Open a modal with a preview of most important information about this product', 'babydufy'),
				'std'  => true,
			],
			[
				'type' => 'heading',
				'name' => esc_html__('Partner stores', 'babydufy'),
				'desc' => esc_html__('External links from partner stores', 'babydufy'),
			],
			[
				'type' => 'url',
				'name' => 'Americanas',
				'id'   => $prefix . 'partner_store_americanas',
				'std'  => 'https://www.americanas.com.br/busca/baby-dufy?chave_search=achistory',
			],
			[
				'type' => 'url',
				'name' => 'Magazine Luiza',
				'id'   => $prefix . 'partner_store_magazineluiza',
				'std'  => 'https://www.magazineluiza.com.br/busca/baby+dufy/',
			],
			[
				'type' => 'url',
				'name' => 'Mercado Livre',
				'id'   => $prefix . 'partner_store_mercadolivre',
				'std'  => 'https://lista.mercadolivre.com.br/bebes/baby-dufy_NoIndex_True?#applied_filter_id%3Dcategory%26applied_filter_name%3DCategorias%26applied_filter_order%3D2%26applied_value_id%3DMLB1384%26applied_value_name%3DBeb%C3%AAs%26applied_value_order%3D2%26applied_value_results%3D29%26is_custom%3Dfalse',
			],
			[
				'type' => 'url',
				'name' => 'Shopee',
				'id'   => $prefix . 'partner_store_shopee',
				'std'  => 'https://shopee.com.br/search?keyword=baby%20dufy&page=0',
			],
			[
				'type' => 'url',
				'name' => 'Submarino',
				'id'   => $prefix . 'partner_store_submarino',
				'std'  => 'https://www.submarino.com.br/busca/baby-dufy',
			],
		],
	];

	return $meta_boxes;
}
