<?php

/**
 * Generic template part to display publication
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php the_content(); ?>
	</div>
	<!-- /.entry-content -->

	<?php if (class_exists('WooCommerce') && !is_product()) : ?>
		<footer class="entry-footer">
			<hr>
			<p>
				<?php _e('Posted in', 'babydufy'); ?>
				<?php the_date(); ?>
				<br>
				<?php _e('Categories:', 'babydufy'); ?>
				<?php the_category(', '); ?>
				<br>
				<?php if (has_tag()) : ?>
					<?php _e('Tags:', 'babydufy'); ?>
					<?php the_tags(' ', ' ', ' '); ?>
				<?php endif; ?>
			</p>
			<hr>
		</footer>
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
