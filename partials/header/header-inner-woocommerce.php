<?php

/**
 * Template part
 *
 * WooCommerce Inner Header
 */

?>

<div class="header-inner" style="
	background-image: url( <?php header_image(); ?>	);
	color: <?php echo '#' . get_header_textcolor(); ?>;
">
	<div class="container">
		<div class="woocommerce-products-header">
			<?php if (apply_filters('woocommerce_show_page_title', true)) : ?>
				<h1 class="woocommerce-products-header__title header-title"><?php woocommerce_page_title(); ?></h1>
			<?php endif; ?>

			<?php
			/**
			 * Hook: woocommerce_archive_description.
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			do_action('woocommerce_archive_description');
			?>
		</div>
	</div>
	<!-- /.container -->
</div>
<!-- /.header-inner -->
