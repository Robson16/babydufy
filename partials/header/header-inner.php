<?php

/**
 * Template part
 *
 * Default Inner Header
 */

?>

<div class="header-inner" style="
	background-image: url( <?php header_image(); ?>	);
	color: <?php echo '#' . get_header_textcolor(); ?>;
">
	<div class="container">
		<?php
		if (is_search()) {
			echo sprintf(
				'<span class="header-title">%s <span>%s</span></span>',
				__('Search results for:', 'babydufy'),
				get_search_query()
			);
		}

		if (is_archive()) echo sprintf('<h1 class="header-title">%s</h1>', single_term_title("", false));
		if (is_home()) echo '<h1 class="header-title">Blog</h1>';
		else echo sprintf('<h1 class="header-title">%s</h1>', get_the_title());
		?>
	</div>
	<!-- /.container -->
</div>
<!-- /.header-inner -->
