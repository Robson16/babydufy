<?php

/**
 * Modal Product Preview
 */
?>

<div class="modal-product-preview">
	<div class="modal-product-preview__overlay"></div>

	<div class="modal-product-preview__box">
		<span class="modal-product-preview__close">
			<div class="leftright"></div>
			<div class="rightleft"></div>
			<label class="close">
				<?php _e('Close', 'babydufy'); ?>
			</label>
		</span>

		<div class="modal-product-preview__box-content">
			<img class="modal-product-preview__image" src="https://via.placeholder.com/300x300" alt="placeholder">

			<h2 class="modal-product-preview__title"></h2>

			<span>
				<?php _e('Reference/Code: ', 'babydufy'); ?>
				<span class="modal-product-preview__sku"></span>
			</span>

			<hr>

			<p class="modal-product-preview__description"></p>

			<div class="modal-product-preview__buy">
				<span><?php _e('Are you a shopkeeper?', 'babydufy'); ?></span>
				<a class="modal-product-preview__buy-button" href="#" target="_blank">
					<?php _e('Buy here', 'babydufy'); ?>
				</a>
			</div>

			<hr>

			<h3 class="modal-product-preview__title-partner-stores">
				<p>
					<?php _e('Final costumer?', 'babydufy'); ?>
				</p>
				<p>
					<?php _e('Shop at our partner stores', 'babydufy'); ?>
				</p>
			</h3>

			<div class="modal-product-preview__partner-stores"></div>
		</div>
	</div>
</div>
