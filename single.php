<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 */
get_header();
?>

<main>
	<?php
	while (have_posts()) {
		the_post();
		get_template_part('partials/content/content', get_post_format());
	}
	?>

	<div class="container">
		<?php
		if (
			(comments_open() || get_comments_number()) &&
			(class_exists('WooCommerce') && !is_product())
		) {
			comments_template();
		}

		get_template_part('partials/content/content', 'related');
		?>
	</div>
	<!-- /.container -->
</main>

<?php
get_footer();
