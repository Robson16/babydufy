export default class ModalProductPreview {
	constructor() {
		this.targetProductId = undefined;
		this.currentProduct = undefined;

		this.modal = document.querySelector(".modal-product-preview");
		this.productsWithPreview = document.querySelectorAll(".woocommerce-loop-product__link.open-preview");
		this.events();
	}

	// Events Triggers
	events() {
		this.handleClickOnProductWithPreview();
		this.handleCloseModal();
	}

	handleClickOnProductWithPreview() {
		this.productsWithPreview.forEach((product) => {
			product.addEventListener("click", async (event) => {
				event.preventDefault();

				this.targetProductId = product.dataset.product_id;

				if (!this.currentProduct || this.currentProduct.id !== this.targetProductId) {
					await this.getProductData(this.targetProductId);
				}

				this.populateModalWithData();
				this.openModal();
			});
		});
	}

	handleCloseModal() {
		if (this.modal) {
			const modalOverlay = this.modal.querySelector(".modal-product-preview__overlay");
			const modalCloseBtn = this.modal.querySelector(".modal-product-preview__close");

			[modalOverlay, modalCloseBtn].forEach((element) => {
				element.addEventListener("click", (event) => {
					this.closeModal();
				});
			});
		}
	}

	async getProductData(productId) {
		await fetch(`/wp-json/babydufy/v1/product/${productId}`)
			.then((response) => response.json())
			.then((response) => {
				this.currentProduct = response.data;
			})
			.catch((error) => {
				console.error(error);
			});
	}

	openModal() {
		if (this.modal) {
			this.modal.classList.add("open");
		}
	}

	closeModal() {
		if (this.modal && this.modal.classList.contains("open")) {
			this.modal.classList.remove("open");
		}
	}

	populateModalWithData() {
		if (this.modal && this.currentProduct) {
			const modalImage = this.modal.querySelector(".modal-product-preview__image");
			modalImage.src = this.currentProduct.image_url;
			modalImage.alt = this.currentProduct.name;

			this.modal.querySelector(".modal-product-preview__title").innerHTML = this.currentProduct.name;

			this.modal.querySelector(".modal-product-preview__sku").innerHTML = this.currentProduct.sku;

			this.modal.querySelector(".modal-product-preview__description").innerHTML = this.currentProduct.short_description;

			const buyButton = this.modal.querySelector(".modal-product-preview__buy-button");
			buyButton.href = this.currentProduct.add_to_cart_url;
			buyButton.innerHTML = this.currentProduct.add_to_cart_text;

			const partnerStores = this.modal.querySelector(".modal-product-preview__partner-stores");
			partnerStores.innerHTML = "";

			this.currentProduct.partner_stores.forEach((store) => {
				let link = document.createElement("a");

				link.href = store.link;
				link.target = "_blank";
				link.rel = "noopener";

				let logo = document.createElement("img");

				logo.src = store.logo
				logo.alt = store.name;
				logo.title = store.name;

				link.appendChild(logo);

				partnerStores.appendChild(link);
			});
		}
	}
}

